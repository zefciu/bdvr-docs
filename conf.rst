=============================================
Opis konfiguracji aplikacji BEDEVERE
=============================================

----------------
Korzeń
----------------

Korzeniem aplikacji jest słownik z następującymi kluczami:

``meta`` 
    parametry wpływające na interpretację konfiguracji.

``siteconfig`` 
    parametry ogólne strony

``rtypes`` 
    definicje typów zasobów

-------------------------
metaparametry
-------------------------

Dostępne są następujące metaparametry:

``autotranslate`` 
    sprawia, że parametry wyświetlane dla użytkownika są
    automatycznie zawijane w instrukcję ``gettext``

-------------------------------
Konfiguracja strony
-------------------------------

Konfiguracja strony zawiera:


``module`` 
    nazwa modułu pythona, który zostanie utworzony dla strony i w
    którym będzie można umieszczać ew. dodatkową logikę

``title`` 
    wyświetlany tytuł strony


----------------------------------
Konfiguracja typów
----------------------------------

Wyrażenia
""""""""""""""""""""

Predykaty mają formę łańcuchów znaków w specjalnym minijęzyku. Język ten
zawiera:

Zmienne kontekstu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Zmienne kontekstu zaczynają się od znaku kropki symbolizującego kontekst. Są
one oddzieloną kropkami ścieżką do określonego pola.

Zmienne systemowe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Zmienne systemowe zaczynają się od znaku dolara. Mają one formę
``przestrzen_nazw.nazwa_zmiennej``. Zmienne dostarczane przez rdzeń należą do
przestrzeni nazw ``bdvr``. Rozszerzenia powinny mieć własne przestrzenie nazw.

``$this``
    zmienna ta występuje w predykatach opcji ``narrow`` pól ``2many``. Wskazuje
    ona na dany zasób (kontekstem jest zasób powiązany).

``$bdvr.current_user`` - aktualnie zalogowany użytkownik

``$bdvr.now`` - aktualny czas

Operatory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``+ - \* \/`` 
    standardowe operatory arytmentyczne

Stałe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Można używać w predykatach stałych liczbowych i stałych znakowych, jeśli nie
zawierają one znaków spoza znaków alfanumerycznych, białych znaków i
podkreślenia. Znak podkreślenia zastępuje znak ``-`` w slugach.

Predykaty
"""""""""""""""""""""

Predykaty charakteryzują się obecnością jednego lub więcej z następujących
operatorów:

``?t`` 
    operator bezargumentowy - predykat który zawsze jest spełniony

``?f`` 
    operator bezargumentowy - predykat który nigdy nie jest spełniony

``=`` 
    operator równości. Wykonuje różne koercje. Np. zasób może być porównany
    z id lub slugiem.

``?a`` 
    operator dwuargumentowy, którego lewą stronę stanowi pole typu
    2 many, a prawą - inny predykat testowany w kontekście powiązanego typu.
    Predykat jest spełniony, gdy wszystkie powiązane zasoby spełniają predykat.
    W przypadku braku tych zasobów - predykat jest *spełniony*.

``?e`` 
    operator dwuargumentowy o składni identycznej z operatorem
    ``?a``. Jest spełniony, gdy dowolny powiązany zasób spełnia predykat. W
    przypadku braku zasobów powiązanych predykat jest **niespełniony**. Aby
    sprawdzić, czy istnieją w ogóle zasoby powiązane można użyć idiomu::

        .nazwapola ?e ?t

``?m`` 
    operator dwuargumentowy przyjmujący jako argumenty pole i
    wyrażenie regularne. UWAGA! Predykat ten będzie wymagał optymalizacji
    po stronie bazy danych. Wyrażenie regularne **musi być stałą**.

``@`` 
    predykat sprawdzający status zasobu. Forma ``@STAN`` jest cukrem
    syntaktycznym dla formy ``.__status__ = STAN``.

``& | !`` 
    operatory logiczne łączące predykaty


Struktura definicji typu
"""""""""""""""""""""""""""""""""

Definicja typu to element słownika ``rtypes``. Kluczem tego elementu jest nazwa
wewnętrzna typu. Wartością jest słownik o następujących opcjonalnych kluczach:

``fields``
    definicje pól typu

``notifiers``
    lista powiadomień związanych z tym typem zasobu. Odnośniki do
    tych powiadomień znajdą się w konfiguracji ``workflow``
 
``workflow`` 
    definicja cyklu życia
 
``acl``
    kontrola dostępu do całych obiektów typu

``inherit``
    typ bazowy dla danego

``dispname``
    nazwa wyświetlana typu. Jeden element, lub lista dwóch/trzech
    elementów (liczba pojedyncza, liczba mnoga, dopełniacz liczby mnogiej)
    (pluralizacja w językach słowiańskich).

``ensure``
    lista obiektów, których istnienie jest ważne dla poprawnego
    działania aplikacji. Powinny być one utworzone w momencie migracji. Typy
    tych obiektów muszą zawierać jakieś pole unikalne.

Pola
~~~~~~~~~~~~~~~~~~~~~~~~~~

Rdzeń BEDEVERE dostarcza następujące pola:

``string``
    jednolinijkowe pole tekstowe

``text``
    wielolinijkowe pole tekstowe

``html``
    pole HTML w przeglądarce obsługiwane jako WYSIWYG

``slug``
    pole zawierające łańcuch składający się ze znaków alfanumerycznych i
    myślników. Opcja konfiguracyjna ``slug_from`` wskazuje pole, na którego
    podstawie generowana jest sugerowana wartość pola

``action``
    pole akcji wyświetlane jest jako przycisk na liście zasobów i w
    widoku zasobu. Pole to zwraca ``True`` wtedy, gdy przejście cyklu życia
    dokonuje się w wyniku jego przyciśnięcia.

``submitaction``
    pole akcji wysłania formularza edycji zasobu. Jeśli brak
    jest tych pól, zostaje wyświetlony jeden przycisk 'Zapisz'.

``one2one``, ``one2many``, ``many2one``, ``many2many``
    pola relacji zasobów.

Opcje konfiguracyjne pól
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pola konfigurujemy jako słownik zawierający jako klucze nazwy pól, a jako
wartości słowniki z kluczami podanymi niżej. Specjalny nazwa ``__default__``
oznacza konfigurację domyślną.

``type``
    nazwa typu pola

``label``
    wyświetlana nazwa pola

``acl``
    Uprawnienia dla pola

``show_if``
    Predykat, który musi być spełniony, by dane pole było dostępne

``validate``
    Może mieć dwie formy:
        * Słownika zawierającego opcje walidacji
        * Listy dwuelementowych list, w których pierwszym elementem jest
          predykat, a drugim - słownik z opcjami walidacji. Lista przeglądana
          jest sekwencyjnie. Jeśli predykat jest spełniony, konfiguracja
          walidacji zostanie użyta.

``reltype``
    Typ zasobu po drugiej stronie relacji.

``backref``
    Nazwa pola, które umożliwia dotarcie do tego zasobu z drugiej strony
    relacji. Pole to zostanie utworzone z domyślną konfiguracją, jeśli nie
    istnieje, ale może też być skonfigurowane *explicite*

Konfiguracja cyklu życia
-----------------------------

Cykl życia skonfigurowany jest w formie listy. Każdy element listy reprezentuje
możliwe przejście w cyklu. Jest on słownikiem o następujących kluczach.

``from``
    nazwa stanu lub lista nazw stanów, z których może nastąpić dane
    przejście.

``on``
    predykat, który musi być spełniony, by nastąpiło przejście

``to``
    stan wyjściowy przejścia

``notifiers``
    nazwa powiadomienia, lub lista nazw powiadomień, które mają być
    wysłane w momencie przejścia

``set``
    lista par 'ścieżka do pola - wyrażenie', które mają zostać ustawione na
    zasobie

``call``
    nazwa funkcji pythona, która ma być wywołana po przejściu
