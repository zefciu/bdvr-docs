=============================================================================
BEDEVERE - założenia projektu
=============================================================================

---------------------------------
1. Cele Projektu BEDEVERE
---------------------------------

Naszym celem jest
""""""""""""""""""""""

#. Stworzenie *frameworka* aplikacji zarządzania zasobami.
#. Umożliwienie tworzenia aplikacji przy użyciu deklaratywnej konfiguracji przy
   minimalizacji konieczności tworzenia nowego kodu Pythona dla aplikacji
#. Stworzenie metamodelu przechowywania danych i ich przepływu, który będzie
   miał zastosowanie w różnych typach aplikacji (CRM, obsługa *ticketów*,
   zarządzanie wiedzą, zarządzanie treścią, księgowość).
#. Budowa uniwersalnego *frontendu* webowego zdolnego obsłużyć różne
   konfiguracje na podstawie ich modelu danych.
#. Stabilność, przewidywalność, bezpieczeństwo aplikacji.

Naszym celem nie jest
""""""""""""""""""""""""""

#. Zapewnienie użytkownikom dużych możliwości dostosowania *look'n'feel*
   aplikacji webowej.
#. Umożliwienie tworzenia złożonej imperatywnej logiki nie pasującej do
   metamodelu

Problemy, których chcemy szczególnie uniknąć
"""""""""""""""""""""""""""""""""""""""""""""""""""""

#. Przeładowany możnościami i niestabilny rdzeń o niskiej jakości kodu
#. Wiązanie siły roboczej programistów drobnymi poprawkami
#. Konieczność konfigurowania tej samej logiki osobno dla serwera i
   przeglądarki


----------------------------------------
2. Zasób
----------------------------------------

Pojęcie *zasobu* jest centralną koncepcją systemu BEDEVERE. Zasób posiada
następujące cechy:

#. Jest trwałą strukturą danych
#. Posiada URL
#. Należy do określonego typu
#. Znajduje się w określonym stanie

2.1. Typ zasobu
""""""""""""""""""""""""

#. Jest deklaratywnie zdefiniowany
#. Ma zdefiniowany *cykl życia* dla zasobów będących jego instancjami 
   (więcej w punkcje 4)
#. Ma określoną logikę związaną z *cyklem życia*
#. Ma zdefiniowane uprawnienia dostępu (więcej w punkcie 5)


-------------------------------------------
3. Wyrażenia i predykaty
-------------------------------------------

Wyrażenia
""""""""""""""""""""""""
Wyrażenia są konstrukcjami zawierającymi:

#. Odnośniki do pól zasobu zwanego kontekstem wyrażenia
#. Zmienne systemowe (np. aktualna data, użytkownik etc.)
#. Stałe
#. Operatory

Predykaty
""""""""""""""""""""""""

Predykat jest szczególnym rodzajem wyrażenia.  Predykat dla
danego zasobu może być spełniony lub niespełniony.
Np. "Grupa autora zasobu to 'ADMIN'", "Zasób ma ustawioną datę
ważności na datę przeszłą", "Pilność zasobu mieści się w zbiorze {Krytyczna,
Pilna}"

Mając dany predykat musimy być zdolni do:

* Sprawdzenia czy dany zasób spełnia predykat.
* Stworzenia fragmentu SQL, który użyty jako klauzula `WHERE` wyszuka nam
  wszystkie i tylko te zasoby, które spełniają predykat.
* Stworzenie wyrażenia JavaScript, które umożliwi sprawdzenie, czy formularz
  reprezentujący zasób spełnia predykat oraz dynamiczną reakcję na zmianę tego
  stanu.

WAŻNE: musimy upewnić się, że wszystkie te formy reprezentacji dają nam zawsze
ten sam wynik.

Powyższe cechy dadzą nam następujące możności

* Sprawdzenie pojedynczych zasobów dla zarządzania *cyklem życia*
* Sprawdzanie pojedynczych zasobów dla kontroli dostępu.
* Zawężanie wyników wyszukiwań dla *kontroli dostępu*
* Zawężanie wyników wyszukiwań w innych skonfigurowanych
* Dynamiczną walidację oraz wyświetlanie i ukrywanie pól zależnych w formularzu
  webowym

----------------------------------------------
4. Cykl życia
----------------------------------------------

Cykl życia czyni z z zasobu skończoną maszynę stanów. Poza dwoma stanami
specjalnymi "__NIHILUM__" i "__OBLIVIO__" zdefiniować można dowolną liczbę
innych stanów.

Konfiguracja typu zasobu powinna móc określić:

#. Zestaw stanów, w jakich może się znaleźć zasób.
#. Możliwe zmiany tych stanów w postaci (stan wyjściowy - predykat - stan
   końcowy)
#. Logikę towarzyszącą przejściu. Należy zapewnić możliwość deklaratywnego
   zdefiniowania prostych zmian, jak np. zmiana wartości jakiegoś pola, lub
   utworzenie nowego, powiązanego zasobu. W przypadku trudniejszej logiki
   należy umożliwić wstawienie odnośnika do kodu Pythona.
#. Powinna zostać stworzona *pojedyncza* cykliczna praca crona, która może
   wykonywać zmiany cyklu życia bez uczestnictwa użytkownika.

Nowo stworzony zasób ma stan __NIHILUM__ i po swojej pierwszej edycji powinien
przejść do innego stanu. Przejście zasobu do stanu __OBLIVIO__ natomiast
powoduje jego usunięcie.

----------------------------------------------
5. Kontrola dostępu
----------------------------------------------

Użytkownicy są szczególnym rodzajem zasobu.

Typy zasobów określają uprawnienia w postaci listy kontroli dostępu (*acl*).
Typ może określić trzy rodzaje list dostępu: dla zasobu, domyślną dla pól,
dla konkretnego pola.

Istnieją dwa uprawnienia: VIEW i EDIT. Kreacja i usuwanie zasobów uznawane jest
za szczególny rodzaj zmiany stanu i dotyczą ich te same uprawnienia.

Wpisy na liście kontroli dostępu mają formę czteroelementową. Zawierają:

#. wartość A(llow) lub D(eny)

#. predykat który powinien spełnić użytkownik

#. predykat, który powinien spełnić zasób, do którego użytkownik chce
   uzyskać dostęp 

#. zbiór uprawnień (wartość 'V', 'E', lub 'VE')

Lista kontroli wersji przeglądana jest sekwencyjnie. Zostaje użyty pierwszy
wpis na liście, dla którego oba predykaty i rodzaj uprawnienia pasują.

Dodać należy system *impersonacji*, który umożliwi kontrolowane uzyskanie
uprawnień innego użytkownika.

---------------------------------------------------
6. Powiadomienia
---------------------------------------------------

Powiadomienia są szczególnym rodzajem zasobu. Posiadają one przynajmniej
następujące pola:

* Adresatów
* Tytuł
* Treść
* Priorytet

Użytkownicy konfigurują kanały, którymi mają otrzymywać powiadomienia.
Konfiguracja ta obejmuje ważność. (np. normalne powiadomienia mają prychodzić
zbiorczym codziennym mailem, a powiadomienia pilne być wysyłane  natychmiast
SMS-em i mailem). Należy umożliwić taką konfigurację, która nigdy nie spowoduje
zasypania użytkownika powiadomieniami, wśród których mogą "zaginąć"
powiadomienia istotne.

-----------------------------------------------
7. Typy pól
-----------------------------------------------

#. Dane skalarne (wartości znakowe, liczbowe, daty etc.).
#. Linki między zasobami.
#. Akcje - są reprezentowane przez przyciski. Nie są zapisywane,
   ale stanowią informację dla logiki *cyklu życiowego* (np. "zapisz jako
   draft", "wyślij do akceptacji").
#. Pseudopola (brak danych, utrzymywane tylko dla uprawnień, np. "impersonuj")
#. Dane jednorazowe (biorące udział w logice przejść, ale niezapisywane, np.
   "dodaj wartość").
#. Pola wygenerowane (zawierają logikę w postaci wyrażenia lub wywołania
   funkcji pythona. Możliwe cache'owanie).

----------------------------------------------
8. Struktura aplikacji
----------------------------------------------

8.1. Rdzeń BEDEVERE
"""""""""""""""""""""""""""""""

Rdzeń zawiera: 

* interfejs bazodanowy oraz webowy
* frontend dojo
* parser konfiguracji oraz definicję obiektów w niej zdefiniowanych
* definicję generycznej klasy zasobów
* definicję najważniejszych pól

Rdzeń powinien zawierać kod najwyższej jakości w pełni przetestowany i
udokumentowany. Zmiany rdzenia powinny być wprowadzane ostrożnie i zachowawczo.

8.2. Rozszerzenia
""""""""""""""""""""""""""""""""

Rozszerzenia to opcjonalne paczki mające wielokrotnie zastosowanie w jakiej
klasie aplikacji (np. księgowość). Zawierać one mogą typey zasobów i 
danych. Wymagania stawiane dla kodu mogą być trochę luźniejsze niż dla kodu
rdzenia. Można tworzyć rozszerzenia niestabilne i testowe.

8.3. Konfiguracja deklaratywna
"""""""""""""""""""""""""""""""""

Zestaw plików YAML jest centralnym punktem konkretnej aplikacji. Należy dążyć
do tego, aby jak najwięcel logiki można było wyrazić w sposób deklaratywny.
Pliki te powinny zawierać definicje zasobów, ich struktury danych, *cykl życia*
i acl.

8.4. Opcjonalne deklaracje klas i zachowań
"""""""""""""""""""""""""""""""""""""""""""""""

W przypadku gdy konieczna jest implementacja logiki, której nie można łatwo
wyrazić deklaratywnie możliwe powinno być dodanie pythonowych klas zasobów i
mixinów. Należy tego unikać i starać się tworzyć konfigurowalne klasy
wielokrotnego użytku (oraz gromadzić je w rozszerzeniach).


-----------------------------------------------
9. Dostęp do aplikacji
-----------------------------------------------

9.1 Frontend dojo
"""""""""""""""""""""""""""""

Frontend dojo jest podstawową formą dostępu do aplikacji. Jest to
aplikacja typu *RIA* zawierająca generalne metody dostępu do różnych zasobów
takich jak:

* Tabelaryczny widok zasobu
* Formularz dodawania/edycji
* Tabelaryczna lista wielu zasobów
* Wykresy obrazujące statystyki
* Usługa wyszukiwania

Nie można dopuścić do przeładowania możnościami tej aplikacji.

9.2 Dostęp REST
"""""""""""""""""""""""""""

Do zasobów możliwy jest także dostęp bez frontendu webowego. Powinien
być on stosowany zawsze tam, gdzie potrzeby klienta wykraczają poza to, co
chcemy zapewnić we frontendzie dojo.

Przykład: tworzymy dla klienta system e-commerce. Pracownicy firmy łączą się
przez frontend dojo, gdzie mają szeroki dostęp do zarządzania magazynami,
zamówieniami, księgowości sklepu etc. Klienci łączą się przez osobną aplikację,
która z kolei wysyła i pobiera przez interfejs REST dane potrzebne do
wyświetlenia stron sklepu i złożenia zamówienia.

---------------------------------------------------
10. Model biznesowy
---------------------------------------------------

#. Rdzeń BEDEVERE ma charakter *open source* na liberalnej licencji (z
   obietnicą nie zmienienia jej na licencję typu *copyleft*)
#. Aplikacje dla klientów są własnościowe
#. Rozszerzenia opcjonalnie mogą być własnościowe
#. Jeśli system się upowszechni należy szkolić specjalistów od konfiguracji
