all: bdvr-draft.pdf cms.pdf demo-apps.pdf conf.pdf store.pdf Makefile

bdvr-draft.pdf: bdvr-draft.latex Makefile
	latex -output-format pdf bdvr-draft.latex

cms.pdf: cms.latex Makefile
	latex -output-format pdf cms.latex 

store.pdf: store.latex Makefile
	latex -output-format pdf store.latex 

demo-apps.pdf: demo-apps.latex Makefile
	latex -output-format pdf demo-apps.latex

conf.pdf: conf.latex Makefile
	latex -output-format pdf conf.latex

demo-apps.latex: demo-apps.rst Makefile
	rst2latex.py demo-apps.rst > demo-apps.latex

conf.latex: conf.rst Makefile
	rst2latex.py conf.rst > conf.latex

bdvr-draft.latex: bdvr-draft.rst Makefile
	rst2latex.py bdvr-draft.rst > bdvr-draft.latex

cms.latex: cms.wrong.latex Makefile
	cat cms.wrong.latex | sed 's/\\usepackage\[utf-8\]{inputenc}/\\usepackage\[utf8\]\{inputenc\}\n\\usepackage\[OT4\]\{fontenc\}/' > cms.latex

cms.wrong.latex: cms.yaml
	pygmentize -O full=true,encoding=utf-8 -f latex cms.yaml  > cms.wrong.latex

store.latex: store.wrong.latex Makefile
	cat store.wrong.latex | sed 's/\\usepackage\[utf-8\]{inputenc}/\\usepackage\[utf8\]\{inputenc\}\n\\usepackage\[OT4\]\{fontenc\}/' > store.latex

store.wrong.latex: store.yaml
	pygmentize -O full=true,encoding=utf-8 -f latex store.yaml  > store.wrong.latex
