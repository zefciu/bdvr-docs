=======================================
Aplikacje demo BEDEVERE
=======================================

Cel stworzenia aplikacji demo
==================================

Aplikacje demo powstają w trzech celach:

**Celem badawczym** jest określenie minimalnego zakresu możności, jakimi
powinien dysponować rdzeń BEDEVERE, aby móc obsługiwać podstawowe potrzeby
stojące przed przyszłymi aplikacjami.

**Cel demonstracyjny** polega na zaprezentowaniu, jak popularne typy aplikacji
przekładają się na BRMMM. Ułatwiać mają one zrozumienie zamysłu stojącego za
BEDEVERE i implementacji zgodnie z tym zamysłem.

**Cel testowy** uruchomienie działających aplikacji będzie dowodem kompletności
pierwszej wersji BEDEVERE

Priorytety
================================

Aplikacje demo próbując realizować powyższe cele starają się zachować dużą
prostotę. Staramy się unikać zbyt skomplikowanej logiki, która nie wpłynie
pozytywnie na zrozumiałość konfiguracji. Z drugiej strony staramy się, aby
aplikacje odzwierciedłały rzeczywiste potrzeby - nie stosujemy *placeholderów*.

Specyfikacja CMS
===============================

#. Jako administrator strony chcę dysponować narzędziem do zarządzania treścią.

#. Jako administrator strony chcę wybierać redaktorów i autorów artykułów.

#. Jako autor artykułów chcę możliwość edycji WYSIWYG artykułu, aby móc szybko
   tworzyć i edytować artykuły z bogatym formatowaniem.

#. Jako autor artykułów chcę możliwość zapisania niekompletnego artykułu bez
   wysyłania go do oceny, aby móc przerwać edycję w dowolnym momencie.

#. Jako redaktor chcę być powiadamiany o nowych artykułach i widzieć spis
   artykułów, które wysłano do mojej oceny, aby móc sprawnie reagować na
   nadsyłane propozycje.

#. Jako redaktor chcę mieć możliwość stworzenia wewnętrznych notatek
   dotyczących artykułu.

#. Jako redaktor chcę mieć możliwość odrzucenia artykułu, lub jego odesłania do
   autora w celu naniesienia poprawek.

#. Jako autor chcę otrzymać informacje, jakich konkretnie poprawek oczekuje
   redakcja.

#. Jako redaktor chcę możliwość usunięcia z systemu propozycji artykułów, aby
   móc pozbyć się materiałów niepotrzebnych.

#. Jako redaktor chcę mieć możliwość zatwierdzenia artykułów.

#. Jako czytelnik chcę czytać artykuły zatwierdzone przez redakcję w specjalnym
   portalu. 


Specyfikacja sklepu
==================================

# Jako właściciel sklepu potrzebuję platformę e-commerce.

# Jako właściciel sklepu chcę mieć możliwość edycji produktów oraz mianować
osoby za nią odpowiedzialne.

# Jako właściciel sklepu chcę upoważnić osoby odpowiedzialne za komunikację
z klientem oraz za obsługę magazynu. Chcę im nadać ograniczone uprawnienia, aby
mieć większą kontrolę nad zmianami.

# Jako klient chcę aplikację, która pozwoli mi przeglądać produkty i składać
zamówienie.

# Jako klient chcę czasami otrzymywać fakturę za zakupy.

# Jako klient chcę możliwość korzystania z kilku adresów czasami tego samego do
wysyłki i faktury, a czasami dwóch różnych.

# Jako obsługa klienta potrzebuję zestawienie obecnie złożonych zamówień, aby
móc przejrzeć je i obsłużyć.

# Jako obsługa klienta potrzebuję możliwość przekazywania zamówień do magazynu.

# Jako magazynier potrzebuję zestawienie zamówień, które przeznaczone są do
wysyłki, aby móc je skompletować.

# Jako magazynier potrzebuję możliwość wydrukowania faktury VAT, by dołączyć ją
do skompletowanej paczki.
